package lt.vtvpmc.exam.entities;

import java.util.Date;
import java.util.List;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Client implements Serializable {

	private static final long serialVersionUID = -2566315395134076434L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    private String firstName;
    
    private String lastName;
    
	@Temporal(TemporalType.DATE)
	private Date birthDate;
    
    private String phoneNumber;
    
    private String clientType;

	@OneToMany(orphanRemoval = true, mappedBy = "client", fetch = FetchType.EAGER)
	private List<Inventory> clientInventory;
	
    public Client() {
    }

    public Client(String firstName, String lastName, Date birthDate, String phoneNumber, String clientType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.clientType = clientType;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}



	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public List<Inventory> getClientInventory() {
		return clientInventory;
	}

	public void setClientInventory(List<Inventory> clientInventory) {
		this.clientInventory = clientInventory;
	}
    
	public int inventoryNumber() {
		return clientInventory.size();
	}
	
	
}
