
package lt.vtvpmc.exam.ui;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;
import org.springframework.transaction.annotation.Transactional;

public class InventoryReportBean {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Transactional(readOnly = true)
    public List<Client> topTotalWeight() {
       Query q = entityManager.createQuery("select c from Client c");
//       List<Client> newList = q.getResultList();
//       List<Client> topList = new ArrayList<>();
//       for (int i=0; newList.size() > i; i++) {
//    	   int tempSum = 0;
//    	   
//    	   for (int j=0; newList.get(i).getClientInventory().size() > j; j++) {
//    		   tempSum += newList.get(i).getClientInventory().get(j).getWeight();
//    	   }
//    	   if (topList.get(0) < top  ) {
//        	   topList.add(0, newList.get(i));   
//    	   }
//       }
        
        return q.getResultList();
    }
    
    @Transactional(readOnly = true)
    public List<Client> topTotalQuantity() {
        Query q = entityManager.createQuery("select c from Client c");
        return q.getResultList();
    }
    
    @Transactional(readOnly = true)
    public List<Client> topWeight() {
        Query q = entityManager.createQuery("select c from Client c");
        return q.getResultList();
    }
    
    @Transactional(readOnly = true)
    public List<Client> topQuantity() {
        Query q = entityManager.createQuery("select c from Client c");
        return q.getResultList();
    }

    
}
