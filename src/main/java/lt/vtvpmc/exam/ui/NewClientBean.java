package lt.vtvpmc.exam.ui;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lt.vtvpmc.exam.entities.Client;
import org.springframework.transaction.annotation.Transactional;



public class NewClientBean {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private String clientFirstName;
    private String clientLastName;
	private Date clientBirthDate;
    private String clientPhoneNumber;
    private String clientType;
    
    
    
    @Transactional
    public String save() {
        Client client = new Client(clientFirstName, clientLastName, clientBirthDate, clientPhoneNumber, clientType);
        entityManager.persist(client);
        return "main";
    }



	public EntityManager getEntityManager() {
		return entityManager;
	}



	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}



	public String getClientFirstName() {
		return clientFirstName;
	}



	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}



	public String getClientLastName() {
		return clientLastName;
	}



	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}



	public Date getClientBirthDate() {
		return clientBirthDate;
	}



	public void setClientBirthDate(Date clientBirthDate) {
		this.clientBirthDate = clientBirthDate;
	}



	public String getClientPhoneNumber() {
		return clientPhoneNumber;
	}



	public void setClientPhoneNumber(String clientPhoneNumber) {
		this.clientPhoneNumber = clientPhoneNumber;
	}



	public String getClientType() {
		return clientType;
	}



	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
    
  
    

    
}
