package lt.vtvpmc.exam.ui;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lt.vtvpmc.exam.entities.Inventory;
import org.springframework.transaction.annotation.Transactional;



public class NewInventoryBean {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private String inventoryName;  
    private double inventoryWeight;
    private int inventorySector;
    private Date inventoryDate;
	
    
    @Transactional
    public String save() {
        Inventory inventory = new Inventory(inventoryName, inventoryWeight, inventorySector, inventoryDate);
        entityManager.persist(inventory);
        return "customerView";
    }


	public EntityManager getEntityManager() {
		return entityManager;
	}


	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}


	public String getInventoryName() {
		return inventoryName;
	}


	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}


	public double getInventoryWeight() {
		return inventoryWeight;
	}


	public void setInventoryWeight(double inventoryWeight) {
		this.inventoryWeight = inventoryWeight;
	}


	public int getInventorySector() {
		return inventorySector;
	}


	public void setInventorySector(int inventorySector) {
		this.inventorySector = inventorySector;
	}


	public Date getInventoryDate() {
		return inventoryDate;
	}


	public void setInventoryDate(Date inventoryDate) {
		this.inventoryDate = inventoryDate;
	}



    
}
