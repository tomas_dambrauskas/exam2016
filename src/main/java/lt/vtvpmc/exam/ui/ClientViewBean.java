
package lt.vtvpmc.exam.ui;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Inventory;

import org.springframework.transaction.annotation.Transactional;

public class ClientViewBean {
    Client client;
	
	
    @PersistenceContext
    private EntityManager entityManager;
    
    @Transactional(readOnly = true)
    public List<Client> getInventoryList(Client currentClient) {
        Query q = entityManager.createQuery("select i from Inventory i where i.client like :currentClient")
        .setParameter("currentClient", currentClient);
        return q.getResultList();
    }

    @Transactional
    public void removeInventory(Inventory inventory) {
        entityManager.remove(entityManager.merge(inventory));
    }


	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
    

    
}
